import React from 'react';
import { department_data } from './select-data.js';

function ResourceClickYes() {
    if (document.getElementById("availresource3").checked) {
        document.getElementById("availresource1").checked = false;
        document.getElementById("availresource2").checked = false;
        document.getElementById("availresource4").checked = false;
    }
}

function ResourceClickNo() {
    if (document.getElementById("availresource1").checked || document.getElementById("availresource2").checked || document.getElementById("availresource4").checked) {
        document.getElementById("availresource3").checked = false;
    }
}

function collabcheck() {
    if (document.getElementById('collabyes').checked) {
        document.getElementById('collabopen').style.display = 'inline-block';
    }
    else document.getElementById('collabopen').style.display = 'none';
    // document.getElementByID('uploadcell').style.border = 'none';
}

export default function Resouces() {
    return (
        <section className="resources">
            <h3> Your Resources </h3><br></br>
            <i>Tell us about your resource availability to support this project</i>
            <table>
                <tbody>
                    <tr>
                        <td>
                            <div id="resources_available">
                                <b>What resources do you have to support this request?</b><br></br>
                                <fieldset>
                                    <input type="checkbox" id="availresource1" onClick={ResourceClickNo}
                                        name="availresource1" value="Monetary" />
                                    <label>Monetary</label><br></br>
                                    <input type="checkbox" id="availresource2" onClick={ResourceClickNo}
                                        name="availresource2" value="In-kind resources" />
                                    <label>In-kind resources
                                        <i><font size="2">e.g.: subject matter knowledge, staff time, etc.</font></i>
                                    </label>
                                    <br></br>
                                    <input type="checkbox" id="availresource3" onClick={ResourceClickYes}
                                        name="availresource3" value="No resources available" />
                                    <label>No resources available</label>
                                    <br></br>
                                    <input type="checkbox" id="availresource4" onClick={ResourceClickNo}
                                        name="availresource4" value="Unsure" />
                                    <label>Unsure at this time</label>
                                    <br></br>
                                </fieldset>
                                <i><font size="2">Select all that apply</font></i>
                            </div>
                        </td>
                        <td>
                            <div id="approval_level">
                                <b>Is your Director aware of this data needs request?</b><br></br>
                                <input type="radio" id="Yes" name="approval" value="Yes" />
                                <label>Yes</label><br></br>
                                <input type="radio" id="No" name="approval" value="No" />
                                <label>No</label><br></br>
                                <input type="radio" id="DK" name="approval" value="Unsure" />
                                <label>Unsure</label><br></br>
                                <i><font size="2">If you are a Director or above, have you discussed this with your immediate supervisor?</font></i>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br></br>
            <div className="collab_group">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <div className="uploadspecs">
                                    <b>Are you working with another department to meet this data need?</b><br></br>
                                    <label><input type="radio" onClick={collabcheck} id="collabyes" name="collab" value="Yes" />Yes</label><br></br>
                                    <label><input type="radio" id="collabno" onClick={collabcheck} name="collab" value="No" /> No</label><br></br>                                  <font size="2">e.g.: co-ordinating, partnering, or sharing resources</font>
                                </div>
                            </td>
                            <td className="uploadnoborder">
                                <div id="collabopen" style={{ display: "none" }}>
                                    <b>Which department(s)?</b> <br></br>
                                    <select id="collab_dep" name="collab_dep[]">
                                        {department_data.map((element) => (
                                            <option key={element.value} value={element.value}>{element.label}</option>
                                        ))}
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    )
}
