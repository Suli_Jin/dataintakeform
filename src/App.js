import React from 'react';
import Contact from './Contact.js';
import Topics from './Topics.js';
import Resouces from './Resources.js';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <div className="logo">
          <br />
          <img src="images/STC_LOGO.svg" alt="" style={{ width: "200px", height: "45px" }} />
          {/* <input title="" className="custom-search-box" placeholder="Search website" type="search" id="wb-srch-q" name="q"
            value="" size="27" maxlength="150" /> */}
          <button className="btnfake"><i className="fas fa-search"></i></button>
        </div>
        <header>
          <div className="containerhead">
            <nav>
              <table className="nav">
                <tbody>
                  <tr>
                    <td>Subjects</td>
                    <td>Data</td>
                    <td>Analysis</td>
                    <td>Reference</td>
                    <td>Geography</td>
                    <td>Census</td>
                    <td>Surveys and statistical programs</td>
                    <td>About StatCan</td>
                    <td>Canada.ca</td>
                  </tr>
                </tbody>
              </table>
            </nav>
          </div>
        </header>
        <br></br>
        <div className="pg_title" style={{ borderBottom: "1px solid #af3c43" }} >
          <h1>Statistics Canada Data Needs Intake Form</h1>
        </div>
        <section className="box">
          <i className="fas fa-info-circle"></i>
          <h2>Attention: This is a proof of concept</h2>
          <p>
            This webpage is a proof of concpet for a GoC-facing data needs intake form. The purpose of this form is to
            obtain real-time data needs from relevant stakeholders at all levels in participating organizations.
            In doing so, a more accurate and timely reflection of governmental data needs will be possible, facilitating
            agency-wide organizational data development, planning, and coordination.
          </p>
        </section>
        <Contact />
        <br></br>
        <Topics />
        <br></br>
        <Resouces />
        <br></br>
        <br></br>
        <p>
          <input type="reset" name="btnreset" size="50px" value="Reset" /><input type="submit" name="btnsubmit" size="50px" value="Submit" />
        </p>
      </div >
    );
  }
}
export default App;

