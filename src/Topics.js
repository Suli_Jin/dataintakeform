import React from 'react';
import { Subject1_data, secondary_subject_data ,population_data, geography_data, refperiod_data } from './select-data.js';
import $ from 'jquery';

function changeSubject(event) {
  let secondary_subject_options = secondary_subject_data[event.target.value].split(';');
  let secondChoice = $("#subject2");
  secondChoice.empty();
  $.each(secondary_subject_options, function (index, value) {
    secondChoice.append("<option>" + value + "</option>");
  });
}

function yesnoCheck() {
  if (document.getElementById('yesCheck').checked) {
    document.getElementById('uploadyes').style.display = 'inline-block';
  }
  else document.getElementById('uploadyes').style.display = 'none';
  // document.getElementByID('uploadcell').style.border = 'none';
}

export default function Topics() {

  return (
    <section className="topics">
      <h3> Your Data Needs </h3><br></br>
      <i>Start by telling us about what kind of data you are looking for</i>
      <table>
        <tbody>
          <tr>
            <td>
              <div id="Subject1" width="400px"> <b>Subject:</b><br></br>
                <select id="subject" onChange={changeSubject} name="subject">
                  {Subject1_data.map((element) => (
                    <option key={element.value} value={element.value}>{element.label}</option>
                  ))}

                </select>
              </div><br></br>
            </td>
            <td>
              <div> <b>Secondary Subject: </b><br></br>
                <select id="subject2" name="subject2">
                  <option>Select a secondary subject</option>
                </select>
              </div><br></br>
            </td>
            <td>
              <div id="OtherSubject"> <b>Other subject: </b><br></br>
                <input type="text" name="othersubject" size="50px" />
              </div>
              <br></br>
            </td>
          </tr>
        </tbody>
      </table>
      <br></br>
      <table>
        <tbody>
          <tr>
            <td>
              <div id="Population"> <b>Population or unit of interest: </b><br></br>
                <select id="population" name="population">
                  {population_data.map((element) => (
                    <option key={element.value} value={element.value}>{element.label}</option>
                  ))}
                </select>
              </div><br></br>
            </td>
            <td>
              <div id="OtherPop"> <b>Other population or unit of interest: </b><br></br>
                <input type="text" name="otherpop" size="50px" />
              </div>
              <br></br>
            </td>
          </tr>
        </tbody>
      </table>
      <br></br>
      <table>
        <tbody>
          <tr>
            <td>
              <div id="Geography"> <b>Geography of interest: </b><br></br>
                <select id="geography" name="geography">
                  {geography_data.map((element) => (
                    <option key={element.value} value={element.value}>{element.label}</option>
                  ))}
                </select>
              </div><br></br>
            </td>
            <td>
              <div id="OtherGeo"> <b>Other geography of interest: </b><br></br>
                <input type="text" name="othergeo" size="50px" />
              </div>
              <br></br>
            </td>
          </tr>
        </tbody>
      </table>
      <br></br>
      <div className="refgap">
        <div className="ReferencePeriod">
          <table>
            <tbody>
              <tr>
                <td> <b>Reference Period: </b><br></br>
                  <select id="refperiod" name="refperiod">
                    {refperiod_data.map((element) => (
                      <option key={element.value} value={element.value}>{element.label}</option>
                    ))}

                  </select>
                  <br></br><i>
                    <font size="2">How current do you need the data to be?</font>
                  </i>
                </td>
                <td>
                  <div id="OtherRef"> <b>Other reference period: </b><br></br>
                    <input type="text" name="otherref" size="50px" />
                  </div>
                 </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="datatypes">
          <b>What type of data are you looking for?</b><br></br>
          <label><input type="radio" id="crossectional" name="datatype" value="Cross-sectional" />Cross-sectional</label><br></br>
          <label><input type="radio" id="longitudinal" name="datatype" value="Longitudinal" />Longitudinal</label><br></br>
          <label><input type="radio" id="panel" name="datatype" value="Panel" />Panel</label><br></br>
          <label><input type="radio" id="DK" name="datatype" value="Doesn't Matter/All of the above" />Doesn't Matter / All of the above</label>
        </div>
      </div>
      <br></br>
      <div className="upload">
        <table>
          <tbody>
            <tr>
              <td>
                <div className="uploadspecs">
                  <b>Do you have formal data needs documents?</b><br></br>
                  <label>
                    <input type="radio" onClick={yesnoCheck} id="yesCheck" name="uploadspec" value="Yes" />
                      Yes
                    </label>
                  <br></br>
                  <label> <input type="radio" id="NoCheck" onClick={yesnoCheck} name="uploadspec"
                    value="No" />No</label><br></br><i>
                    <font size="2">e.g.: draft questions, a project proposal, a feasibility study</font>
                  </i>
                </div>
              </td>
              <td className="uploadnoborder">
                <div id="uploadyes" style={{ display: "none" }}>
                  <b>Upload your data needs document:</b>
                  <br></br>
                  <input type="file" id="questions" name="questions" />
                </div>
              </td>
              <td className="uploadnoborder">
                <div className="uploadspecs">
                  <b>Is this for a known data gap?</b><br></br>
                  <input type="radio" id="Yes" name="datagap" value="Yes" />
                  <label>Yes</label><br></br>
                  <input type="radio" id="No" name="datagap" value="No" />
                  <label>No</label><br></br>
                  <input type="radio" id="DK" name="datagap" value="Unsure" />
                  <label>Unsure</label>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>
  )
}


