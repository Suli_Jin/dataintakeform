import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
// class Doc extends React.Component{
//   componentDidMount(){
//     document.title = "Statistics Canada Data Needs Intake"
//   }

//   render(){
//     return(
//       <b> test </b>
//     )
//   }
// }

// ReactDOM.render(
//   <Doc />,
//   document.getElementById('root')
// );
// class IntakeForm 
// ReactDOM.render(<h1>Statistics Canada Data Needs Intake</h1>,
//   document.getElementById('root')
// )
// ReactDOM.render(<h2>Attention: This is a proof of concept</h2>,
//   document.getElementById('root')
// )

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
