import React from 'react';
import { department_data, occgroup_data, occlevel_data, community_data } from './select-data.js';

export default function Contact() {
  return (
    <section className="cntact">
      <h3> Contact Information </h3><br></br>
      <i>This is optional information. However, if you wish to be contacted regarding your needs, please ensure you
      provide a means for Statistics Canada to contact you
      </i>
      <table>
        <tbody>
          <tr>
            <td>
              <div id="Name"> <b>Name:</b><br></br> <input type="name" name="name" /></div><br></br>
            </td>
            <td>
              <div id="EMAIL"> <b>Email:</b><br></br> <input type="email" name="email" placeholder="e.g.: your.name@domain.com" /> </div><br></br>
            </td>
            <td>
              <div id="PHONE"> <b>Phone #:</b><br></br> <input type="tel" name="phone" placeholder="e.g.: 613-999-9999" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" /></div><br></br>
            </td>
            <td>
              <div className="select-container"><b>Department:</b><br></br>
                <select id="department" name="department" className="select-container">
                  {department_data.map((element) => (
                    <option key={element.value} value={element.value}>{element.label}</option>
                  ))}
                </select>
              </div><br></br>
            </td>
          </tr>
        </tbody>
      </table>
      <br></br>
      <table>
        <tbody>
          <tr>
            <td>
              <div id="JTitle"> <b>Job Title:</b><br></br><input type="name" name="job" /></div><br></br>
            </td>
            <td>
              <div id="Group"> <b>Occupational Group:</b><br></br>
                <select id="occgroup" name="occgroup">
                  {occgroup_data.map((element) => (
                    <option key={element.value} value={element.value}>{element.label}</option>
                  ))}
                </select>
              </div><br></br>
            </td>
            <td>
              <div id="Level"> <b>Occupational Level:</b><br></br>
                <select id="occlevel" name="occlevel">
                  {occlevel_data.map((element) => (
                    <option key={element.value} value={element.value}>{element.label}</option>
                  ))}
                </select>
              </div><br></br>
            </td>
            <td>
              <div id="functional"> <b>What functional community do you represent?</b><br></br>
                <select id="community" name="community" width="400px">
                  {community_data.map((element) => (
                    <option key={element.value} value={element.value}>{element.label}</option>
                  ))}
                </select>
              </div>
              <i><font size="2">If you belong to more than one, pick the one that reflects this request the most</font></i><br></br>
            </td>
          </tr>
        </tbody>
      </table>
      <br></br>
      <div className="cntrow">
        <div id="message">
          <b>Please describe your data needs:</b><br></br>
          <textarea id="msg" name="data_needs" rows="4" cols="100"></textarea>
          <br></br><i>
            <font size="2">For example, what program or project do you need this data for?</font>
          </i>
        </div>
        <div className="wantcontact">
          <b>Do you want to be contacted?</b><br></br>
          <label><input type="radio" id="Yes" name="wantcontact" value="Yes" />Yes</label><br></br>
          <label><input type="radio" id="No" name="wantcontact" value="No" />No</label>
        </div>
      </div>
      <br></br>
    </section>
  );
};